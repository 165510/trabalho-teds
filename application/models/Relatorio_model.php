<?php
  class Relatorio_model extends CI_Model{

		public function __construct(){ }

		public function getTestador($data){
			$this->db->select('t.nome, t.data_criacao, g.nome as grupo');
			$this->db->from('testador t');
			$this->db->join('grupo g', 't.grupo_id=g.grupo_id');

			$where = array('t.data_criacao>='=>$data);
			
			$this->db->where($where);
			
			$query = $this->db->get();
			
			return $query->result_array();
		}

		public function getTeste($data){
			$this->db->select('t.nome, t.situacao, t.data_criacao, tt.nome as testador, f.nome as funcionalidade');
			$this->db->from('teste t');
			$this->db->join('testador tt', 't.testador_id=tt.testador_id');
			$this->db->join('funcionalidade f', 't.funcionalidade_id=f.funcionalidade_id');

			$where = array('t.data_criacao>='=>$data);
			
			$this->db->where($where);
			
			$query = $this->db->get();
			
			return $query->result_array();
		}
  }
 ?>
