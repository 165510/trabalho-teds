<?php
  class Funcionalidade_model extends CI_Model{

	public function __construct(){}

	public function get($id=null){
		if($id==null){
			$this->db->select();
			$query = $this->db->get("funcionalidade");
			return $query->result_array();
		}
		$query = $this->db->get_where('funcionalidade', array('funcionalidade_id'=>$id));
		return $query->row_array();
	}

	public function remover($id){
		return $this->db->where(array('funcionalidade_id'=>$id))->delete('funcionalidade');
	}

	public function cadastrar($id=null){
		$registro = $this->input->post();
		if($id==null){
			return $this->db->insert('funcionalidade', $registro);
		}
		return $this->db->where(array('funcionalidade_id'=>$id))->update('funcionalidade',$registro);
	}
  }
 ?>
