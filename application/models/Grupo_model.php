<?php
  class Grupo_model extends CI_Model{

	public function __construct(){}

	public function get($id=null){
		if($id==null){
			$this->db->select();
			$query = $this->db->get("grupo");
			return $query->result_array();
		}
		$query = $this->db->get_where('grupo', array('grupo_id'=>$id));
		return $query->row_array();
	}

	public function remover($id){
		return $this->db->where(array('grupo_id'=>$id))->delete('grupo');
	}

	public function cadastrar($id=null, $imagePath=null){
		$registro = $this->input->post();
		$registro['imagem'] = $imagePath;

		if($id==null){
			return $this->db->insert('grupo', $registro);
		}
		return $this->db->where(array('grupo_id'=>$id))->update('grupo',$registro);
	}
  }
 ?>
