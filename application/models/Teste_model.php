<?php
  class Teste_model extends CI_Model{
		
		public function __construct(){}

		public function get($id=null){
			if($id==null){
					$this->db->select('t.teste_id, t.nome, t.descricao, t.situacao, t.data_criacao, tt.nome as testador, f.nome as funcionalidade');
					$this->db->from('teste t');
					$this->db->join('testador tt', 't.testador_id=tt.testador_id');
					$this->db->join('funcionalidade f', 't.funcionalidade_id=f.funcionalidade_id');

					$query = $this->db->get();
					return $query->result_array();
			}
			$query = $this->db->get_where('teste', array('teste_id'=>$id));
			return $query->row_array();
		}

		public function remover($id){
			return $this->db->where(array('teste_id'=>$id))->delete('teste');
		}

		public function cadastrar($id=null){
			$registro = $this->input->post();
			if($id==null){
					$registro['data_criacao'] = date('Y-m-d');
					return $this->db->insert('teste', $registro);
			}
			return $this->db->where(array('teste_id'=>$id))->update('teste',$registro);
		}
  }
 ?>
