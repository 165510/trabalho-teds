<?php
  class Testador_model extends CI_Model{
		
		public function __construct(){}

		public function get($id=null){
			if($id==null){
					$this->db->select('t.testador_id, t.nome, t.data_criacao, g.nome as grupo');
					$this->db->from('testador t');
					$this->db->join('grupo g', 't.grupo_id=g.grupo_id');
					$query = $this->db->get();
					return $query->result_array();
			}
			$query = $this->db->get_where('testador', array('testador_id'=>$id));
			return $query->row_array();
		}

		public function remover($id){
			return $this->db->where(array('testador_id'=>$id))->delete('testador');
		}

		public function cadastrar($id=null){
			$registro = $this->input->post();
			if($id==null){
					$registro['data_criacao'] = date('Y-m-d');
					return $this->db->insert('testador', $registro);
			}
			return $this->db->where(array('testador_id'=>$id))->update('testador',$registro);
		}
  }
 ?>
