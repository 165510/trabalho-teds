<?php

  class Relatorio extends MY_Controller{

		public function __construct(){
			parent::__construct();

			$this->load->model('relatorio_model');
		}

		public function testador(){
			$dados['titulo'] = "Relatório de testadores por data de criação";
			
			$this->load->helper('form');
			$this->load->library('form_validation');
			
			$this->template->load('template', 'relatorio/testador/index', $dados);
		}

		public function relatorioTestador() {
			$data = $this->input->post('data_criacao');
			$dados['titulo'] = "Testadores";
			$dados['data']   = $this->relatorio_model->getTestador($data);
			
			$this->load->library('MY_FPDF');
			$this->load->view('relatorio/testador/pdf', $dados);
		}
		

		public function teste(){
			$dados['titulo'] = "Relatório de teste por data de criação";
			
			$this->load->helper('form');
			$this->load->library('form_validation');
			
			$this->template->load('template', 'relatorio/teste/index', $dados);
		}
		
		public function relatorioTeste() {
			$data = $this->input->post('data_criacao');
			$dados['titulo'] = "Teste";
			$dados['data'] = $this->relatorio_model->getTeste($data);

			$this->load->library('MY_FPDF');
			$this->load->view('relatorio/teste/pdf', $dados);
		}
  }
 ?>
