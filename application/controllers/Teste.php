<?php
  class Teste extends MY_Controller{

	public function __construct(){
		parent::__construct();
		
		$this->load->model('teste_model');

	}

	public function index(){
		$dados['titulo']= "Manutenção de Teste";
					
		$dados['lista'] = $this->teste_model->get();
					
		$this->template->load('template', 'teste/index.php', $dados);
	}

	public function remover($id){
		if(!$this->teste_model->remover($id)){
			$this->session->set_userdata('mensagem', 'erro');
		}else{
			$this->session->set_userdata('mensagem', 'sucesso');
		}
		redirect('teste');
	}

	public function cadastrar($id=null){
		$this->load->helper('form');
		$this->load->library('form_validation');

		$dados['titulo'] = "Cadastro de Testes";

		$rule_nome = 'required' . (($id==null)? '|is_unique[teste.nome]' : '');
		$this->form_validation->set_rules('nome', 'Nome', $rule_nome);
		$this->form_validation->set_rules('descricao', 'Descrição', 'required');
		$this->form_validation->set_rules('situacao', 'Situação', 'required');
		$this->form_validation->set_rules('testador_id', 'Testador', 'required');
		$this->form_validation->set_rules('funcionalidade_id', 'Funcionalidade', 'required');





		$dados['acao'] = "teste/cadastrar/";

		$dados['registro'] = null;
		if($id!==null){
			$dados['acao']    .= $id;
			$dados['registro'] = $this->teste_model->get($id);
		}

		$this->load->model('testador_model');
		$this->load->model('funcionalidade_model');

		$dados['listaTestador'] = $this->testador_model->get();
		$dados['listaFuncionalidade'] = $this->funcionalidade_model->get();


		if($this->form_validation->run()===false){

			$this->template->load('template', 'teste/form.php', $dados);
		}else{
			if(!$this->teste_model->cadastrar($id)){
				$this->session->set_userdata('mensagem', 'erro');
			}else{
				$this->session->set_userdata('mensagem', 'sucesso');
			}
			redirect('teste');
		}
	}
  }
 ?>
