<?php
  class Grupo extends MY_Controller{

	public function __construct(){
		parent::__construct();

		$this->load->model('grupo_model');

	}

	public function index(){
		$dados['titulo']= "Manutenção de Grupo";
		$dados['lista'] = $this->grupo_model->get();

		$this->template->load('template', 'grupo/index.php', $dados);
	}

	public function remover($id){
		if(!$this->grupo_model->remover($id)){
			$this->session->set_userdata('mensagem', 'erro');
		}else{
			$this->session->set_userdata('mensagem', 'sucesso');
		}
		redirect('grupo');
	}

	public function cadastrar($id=null){
		$this->load->helper('form');
		$this->load->library('form_validation');

		$dados['titulo'] = "Cadastro de Grupos";

		$rule_nome = 'required' . (($id==null)? '|is_unique[grupo.nome]' : '');
		$this->form_validation->set_rules('nome', 'Nome', $rule_nome);

		$dados['acao'] = "grupo/cadastrar/";

		$dados['registro'] = null;
		if($id!==null){
			$dados['acao']    .= $id;
			$dados['registro'] = $this->grupo_model->get($id);
		}

		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		// $config['max_size'] = 100;
		// $config['max_width'] = 1024;
		// $config['max_height'] = 768;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);



		if($this->form_validation->run()===false){
			$this->template->load('template', 'grupo/form', $dados);
		}else{

      $this->upload->do_upload('imagem');
      $partes = explode("/", $this->upload->data('full_path'));
      $arquivo = end($partes);

			if(!$this->grupo_model->cadastrar($id, $arquivo)){
				$this->template->load('template', 'grupo/form', $dados);
			}
			else{
				$this->session->set_userdata('mensagem', 'sucesso');
			}
			redirect('grupo');

		}
	}
  }
 ?>
