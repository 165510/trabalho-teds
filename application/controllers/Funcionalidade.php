<?php
  class Funcionalidade extends MY_Controller{

	public function __construct(){
		parent::__construct();
		
		$this->load->model('funcionalidade_model');
		
	}

	public function index(){
		$dados['titulo']= "Manutenção de Funcionalidade";
		$dados['lista'] = $this->funcionalidade_model->get();

		$this->template->load('template', 'funcionalidade/index.php', $dados);
	}

	public function remover($id){
		if(!$this->funcionalidade_model->remover($id)){
			$this->session->set_userdata('mensagem', 'erro');
		}else{
			$this->session->set_userdata('mensagem', 'sucesso');
		}
		redirect('funcionalidade');
	}

	public function cadastrar($id=null){
		$this->load->helper('form');
		$this->load->library('form_validation');

		$dados['titulo'] = "Cadastro de Funcionalidades";

		$rule_nome = 'required' . (($id==null)? '|is_unique[funcionalidade.nome]' : '');
		$this->form_validation->set_rules('nome', 'Nome', $rule_nome);

		$dados['acao'] = "funcionalidade/cadastrar/";

		$dados['registro'] = null;
		if($id!==null){
			$dados['acao']    .= $id;
			$dados['registro'] = $this->funcionalidade_model->get($id);
		}

		if($this->form_validation->run()===false){
			$this->template->load('template', 'funcionalidade/form', $dados);
		}else{
			if(!$this->funcionalidade_model->cadastrar($id)){
				$this->session->set_userdata('mensagem', 'erro');
			}else{
				$this->session->set_userdata('mensagem', 'sucesso');
			}
			redirect('funcionalidade');
		}
	}
  }
 ?>
