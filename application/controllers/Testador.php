<?php
  class Testador extends MY_Controller{

	public function __construct(){
		parent::__construct();
		
		$this->load->model('testador_model');

	}

	public function index(){
		$dados['titulo']= "Manutenção de Testador";
					
		$dados['lista'] = $this->testador_model->get();
					
		$this->template->load('template', 'testador/index.php', $dados);
	}

	public function remover($id){
		if(!$this->testador_model->remover($id)){
			$this->session->set_userdata('mensagem', 'erro');
		}else{
			$this->session->set_userdata('mensagem', 'sucesso');
		}
		redirect('testador');
	}

	public function cadastrar($id=null){
		$this->load->helper('form');
		$this->load->library('form_validation');

		$dados['titulo'] = "Cadastro de Testadores";

		$rule_nome = 'required' . (($id==null)? '|is_unique[testador.nome]' : '');
		$this->form_validation->set_rules('nome', 'Nome', $rule_nome);
		$this->form_validation->set_rules('grupo_id', 'Grupo', 'required');

		$dados['acao'] = "testador/cadastrar/";

		$dados['registro'] = null;
		if($id!==null){
			$dados['acao']    .= $id;
			$dados['registro'] = $this->testador_model->get($id);
		}

		$this->load->model('grupo_model');
		$dados['listaGrupo'] = $this->grupo_model->get();

		if($this->form_validation->run()===false){

			$this->template->load('template', 'testador/form.php', $dados);
		}else{
			if(!$this->testador_model->cadastrar($id)){
				$this->session->set_userdata('mensagem', 'erro');
			}else{
				$this->session->set_userdata('mensagem', 'sucesso');
			}
			redirect('testador');
		}
	}
  }
 ?>
