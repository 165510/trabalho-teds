<div class="row col-md-12">
    <div class="box">
        <div class="box-body">
          <?php
              if(validation_errors() != null){ ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Erro!</h4>
                    <?php echo validation_errors(); ?>
                </div>
          <?php } ?>

          <?php echo form_open($acao); ?>
            <div class="form-group">
                <label for="nome">Nome</label>
                <input id="nome" class="form-control" type="text" name="nome"
                value="<?= set_value('nome', $registro['nome']); ?>"
                placeholder="Nome">
            </div>

            <div class="form-group">
              <label for="grupo_id">Grupo</label>
              <select class="form-control" id="grupo_id" name="grupo_id">
                <option value="">Selecione o grupo</option>
                <?php foreach ($listaGrupo as $item): ?>
                    <option value="<?= $item['grupo_id']; ?>" <?php if(isset($registro) && $item['grupo_id']==$registro['grupo_id']) echo "selected";?>>
                        <?= $item['nome']; ?>
                    </option>
                <?php endforeach; ?>
              </select>
            </div>

						<button class="btn btn-success" type="submit">Enviar</button>
						<a href="<?= site_url("testador") ?>" class="btn">Voltar</a>
          </form>
        </div>
    </div>
</div>
