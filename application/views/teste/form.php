<div class="row col-md-12">
    <div class="box">
        <div class="box-body">
          <?php
              if(validation_errors() != null){ ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Erro!</h4>
                    <?php echo validation_errors(); ?>
                </div>
          <?php } ?>

          <?php echo form_open($acao); ?>
            <div class="form-group">
                <label for="nome">Nome</label>
                <input id="nome" class="form-control" type="text" name="nome"
                value="<?= set_value('nome', $registro['nome']); ?>"
                placeholder="Nome">
            </div>

						<div class="form-group">
                <label for="descricao">Descrição</label>
                <input id="descricao" class="form-control" type="text" name="descricao"
                value="<?= set_value('descricao', $registro['descricao']); ?>"
                placeholder="Descrição">
            </div>

						<div class="form-group">
              <label for="situacao">Situação</label>
              <select class="form-control" id="situacao" name="situacao">
                <option value="">Selecione a situação</option>
                    <option value="1" <?php if(isset($registro) && $registro['situacao']== 1) echo "selected";?>>
                        <?= "Andamento"; ?>
                    </option>
										<option value="2" <?php if(isset($registro) && $registro['situacao']== 2) echo "selected";?>>
                        <?= "Finalizado"; ?>
                    </option>
              </select>
            </div>

            <div class="form-group">
              <label for="testador_id">Testador</label>
              <select class="form-control" id="testador_id" name="testador_id">
                <option value="">Selecione o testador</option>
                <?php foreach ($listaTestador as $item): ?>
                    <option value="<?= $item['testador_id']; ?>" <?php if(isset($registro) && $item['testador_id']==$registro['testador_id']) echo "selected";?>>
                        <?= $item['nome']; ?>
                    </option>
                <?php endforeach; ?>
              </select>
            </div>

						<div class="form-group">
              <label for="funcionalidade_id">Funcionalidade</label>
              <select class="form-control" id="funcionalidade_id" name="funcionalidade_id">
                <option value="">Selecione a funcionalidade</option>
                <?php foreach ($listaFuncionalidade as $item): ?>
                    <option value="<?= $item['funcionalidade_id']; ?>" <?php if(isset($registro) && $item['funcionalidade_id']==$registro['funcionalidade_id']) echo "selected";?>>
                        <?= $item['nome']; ?>
                    </option>
                <?php endforeach; ?>
              </select>
            </div>

						<button class="btn btn-success" type="submit">Enviar</button>
						<a href="<?= site_url("teste") ?>" class="btn">Voltar</a>
          </form>
        </div>
    </div>
</div>
