<div class="row col-md-12">
    <div class="box">
        <div class="box-body">
          <?php
              if(validation_errors() != null){ ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Erro!</h4>
                    <?php echo validation_errors(); ?>
                </div>
          <?php } ?>

          <?php
              if(isset($error)){ ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Erro!</h4>
                    <?php echo $error; ?>
                </div>
          <?php } ?>

          <?php echo form_open_multipart($acao); ?>
            <div class="form-group">
                <label for="nome">Nome</label>
                <input id="nome" class="form-control" type="text" name="nome"
                value="<?= set_value('nome', $registro['nome']); ?>"
                placeholder="Nome" >
            </div>
  						<div class="form-group">
                  <label for="imagem">Imagem</label>
                  <input id="imagem" class="form-control" type="file" name="imagem">
              </div>
              
						<button class="btn btn-success" type="submit">Enviar</button>
						<a href="<?= site_url("grupo") ?>" class="btn">Voltar</a>
          </form>
        </div>
    </div>
</div>
