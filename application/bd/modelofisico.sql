CREATE TABLE funcionalidade(
    funcionalidade_id INT(11) NOT NULL,
    nome VARCHAR(50) NOT NULL
); INSERT INTO funcionalidade(funcionalidade_id, nome)
VALUES(2, 'Cadastrar Usuário'),(1, 'Realizar Login');
CREATE TABLE grupo(
    grupo_id INT(11) NOT NULL,
    nome VARCHAR(50) NOT NULL,
    imagem VARCHAR(200) DEFAULT NULL
); INSERT INTO grupo(grupo_id, nome, imagem)
VALUES(1, 'Funcional', 'baixar.png'),(2, 'Performance', 'baixar.png');
CREATE TABLE login(
    id INT(11) NOT NULL,
    nome VARCHAR(50) NOT NULL,
    email VARCHAR(100) NOT NULL,
    senha VARCHAR(32) NOT NULL
); INSERT INTO login(id, nome, email, senha)
VALUES(
    1,
    'Adm',
    'adm@gmail.com',
    MD5('adm')
);
CREATE TABLE testador(
    testador_id INT(11) NOT NULL,
    nome VARCHAR(50) NOT NULL,
    data_criacao DATE NOT NULL,
    grupo_id INT(11) NOT NULL
); INSERT INTO testador(
    testador_id,
    nome,
    data_criacao,
    grupo_id
)
VALUES(1, 'Marcos', '2019-11-16', 1),(2, 'Vinícius', '2019-11-16', 2);
CREATE TABLE teste(
    teste_id INT(11) NOT NULL,
    nome VARCHAR(50) NOT NULL,
    descricao VARCHAR(200) NOT NULL,
    situacao INT(11) NOT NULL,
    data_criacao DATE NOT NULL,
    testador_id INT(11) NOT NULL,
    funcionalidade_id INT(11) NOT NULL
); INSERT INTO teste(
    teste_id,
    nome,
    descricao,
    situacao,
    data_criacao,
    testador_id,
    funcionalidade_id
)
VALUES(
    1,
    'Teste Funcional',
    'Testando...',
    1,
    '2019-11-16',
    1,
    1
),(
    2,
    'Teste de Performance',
    'Testando...',
    2,
    '2019-11-16',
    2,
    2
);
ALTER TABLE
    funcionalidade ADD PRIMARY KEY(funcionalidade_id),
    ADD UNIQUE KEY nome(nome);
ALTER TABLE
    grupo ADD PRIMARY KEY(grupo_id),
    ADD UNIQUE KEY nome(nome);
ALTER TABLE
    login ADD PRIMARY KEY(id),
    ADD UNIQUE KEY email(email),
    ADD UNIQUE KEY senha(senha);
ALTER TABLE
    testador ADD PRIMARY KEY(testador_id),
    ADD KEY grupo_fk(grupo_id);
ALTER TABLE
    teste ADD PRIMARY KEY(teste_id),
    ADD UNIQUE KEY nome(nome),
    ADD KEY funcionalidade_fk(funcionalidade_id),
    ADD KEY testador_fk(testador_id);
ALTER TABLE
    funcionalidade MODIFY funcionalidade_id INT(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 3;
ALTER TABLE
    grupo MODIFY grupo_id INT(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 3;
ALTER TABLE
    login MODIFY id INT(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 2;
ALTER TABLE
    testador MODIFY testador_id INT(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 3;
ALTER TABLE
    teste MODIFY teste_id INT(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 3;
ALTER TABLE
    testador ADD CONSTRAINT grupo_fk FOREIGN KEY(grupo_id) REFERENCES grupo(grupo_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE
    teste ADD CONSTRAINT funcionalidade_fk FOREIGN KEY(funcionalidade_id) REFERENCES funcionalidade(funcionalidade_id) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT testador_fk FOREIGN KEY(testador_id) REFERENCES testador(testador_id) ON DELETE CASCADE ON UPDATE CASCADE;
